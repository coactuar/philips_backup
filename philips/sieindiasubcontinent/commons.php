<!--Audi Modal -->
<div class="modal fade" id="audiSelect" tabindex="-1" role="dialog" aria-labelledby="audiSelectTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="audiSelectLongTitle">Meeting Rooms</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="content scroll">
          <ul class="audis">
            <li><a href="room1.php">Digitalization of WI</a></li>
            <li><a href="room2.php">Labview Tool Automation</a></li>
            <li><a href="room3.php">POSM Aggregator Model</a></li>
            <li><a href="room4.php">Site Feasibility</a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>
<!--Helpdesk-->
<?php
$file = $_SERVER["SCRIPT_NAME"];
$break = Explode('/', $file);
$pfile = $break[count($break) - 1];
?>
<div id="talktous" class="popup-dialog">
  <div class="popup-content">
    <div id="chat_team" class="team_chat_box">
      <div class="chat_history scroll" data-touser="team" id="chat_history_team"></div>
      <form>
        <div class="form-group">
          <input name="chat_message_team" id="chat_message_team" rows="1" class="input sendmsg" autocomplete="off">
        </div>
        <div class="form-group text-left">
          <button type="button" name="send_teamchat" class="send_teamchat btn btn-primary" data-src="<?php echo $pfile ?>" data-to="team" data-from="<?php echo $userid ?>">Send</button>
        </div>
      </form>
    </div>
  </div>
</div>